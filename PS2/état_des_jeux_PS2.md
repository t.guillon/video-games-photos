## À propos

État général et contenu des jeux **PS2**.

États:

 - ME : mauvais état
 - EM : état moyen
 - BE : bon état
 - TBE : très bon état
 - near mint : bah, near mint quoi

Le jugé des états reste subjectif, le mieux est d'aller voir les photos dans le dossier correspondant.

## Description

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: left;">
      <th>Jeu</th>
      <th>Général</th>
      <th>Boite</th>
      <th>Livret</th>
      <th>CD</th>
      <th>Cartouche</th>
      <th>Autre</th>
      <th>VIP</th>
      <th>Photo</th>
      <th>Pas à vendre</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Klonoa 2</td>
      <td>TBE</td>
      <td>TBE (un mini trou dans la couv' plastique sur la tranche)</td>
      <td>TBE</td>
      <td>µrayé</td>
      <td></td>
      <td></td>
      <td></td>
      <td>klonoa2</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Kya Dark Lineage</td>
      <td>BE</td>
      <td>BE</td>
      <td>TBE</td>
      <td>quelques rayures</td>
      <td></td>
      <td></td>
      <td></td>
      <td>kya</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Spinter Cell</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td>splinter</td>
      <td>x</td>
    </tr>
    <tr>
      <td>- Splinter Cell 1</td>
      <td>BE/TBE</td>
      <td>BE</td>
      <td>TBE</td>
      <td>µrayé</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>- Pandora Tomorrow</td>
      <td>BE</td>
      <td>BE</td>
      <td>BE</td>
      <td>µrayé</td>
      <td></td>
      <td>pub</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>- Chaos Theory</td>
      <td>BE/TBE</td>
      <td>TBE</td>
      <td>TBE</td>
      <td>traces de doigt impossibles à enlever (dégueu!)</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>- Double Agent</td>
      <td>BE</td>
      <td>BE</td>
      <td>BE</td>
      <td>µrayé</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
  </tbody>
</table>
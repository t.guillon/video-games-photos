## À propos

État général et contenu des jeux **DS**.

États:

 - ME : mauvais état
 - EM : état moyen
 - BE : bon état
 - TBE : très bon état
 - near mint : bah, near mint quoi

Le jugé des états reste subjectif, le mieux est d'aller voir les photos dans le dossier correspondant.

## Description

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: left;">
      <th>Jeu</th>
      <th>Général</th>
      <th>Boite</th>
      <th>Livret</th>
      <th>CD</th>
      <th>Cartouche</th>
      <th>Autre</th>
      <th>VIP</th>
      <th>Photo</th>
      <th>Pas à vendre</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Trackmania Turbo (PAS DE JEU!)</td>
      <td>TBE</td>
      <td>TBE</td>
      <td>TBE</td>
      <td></td>
      <td></td>
      <td>Infos</td>
      <td></td>
      <td>trackmania</td>
      <td>x</td>
    </tr>
  </tbody>
</table>
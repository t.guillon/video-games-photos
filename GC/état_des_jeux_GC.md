## À propos

État général et contenu des jeux **GC**.

États:

 - ME : mauvais état
 - EM : état moyen
 - BE : bon état
 - TBE : très bon état
 - near mint : bah, near mint quoi

Le jugé des états reste subjectif, le mieux est d'aller voir les photos dans le dossier correspondant.

## Description

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: left;">
      <th>Jeu</th>
      <th>Général</th>
      <th>Boite</th>
      <th>Livret</th>
      <th>CD</th>
      <th>Cartouche</th>
      <th>Autre</th>
      <th>VIP</th>
      <th>Photo</th>
      <th>Pas à vendre</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Battalion Wars</td>
      <td>BE/TBE</td>
      <td>BE (1 peu frottée)</td>
      <td>TBE</td>
      <td>µrayé</td>
      <td></td>
      <td>Pub</td>
      <td>oui</td>
      <td>battalion_wars</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Fzero GX</td>
      <td>Near mint</td>
      <td>TBE (1 mini poc avant haut)</td>
      <td>Nickel</td>
      <td>Nickel</td>
      <td></td>
      <td>Conso, pub</td>
      <td>grattés</td>
      <td>fzero_gx</td>
      <td></td>
    </tr>
    <tr>
      <td>MGS Twin Snakes</td>
      <td>Near mint</td>
      <td>TBE</td>
      <td>Nickel</td>
      <td>Nickel (2 CDs)</td>
      <td></td>
      <td>Conso</td>
      <td></td>
      <td>mgs</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Paper Mario</td>
      <td>TBE</td>
      <td>TBE</td>
      <td>Nickel</td>
      <td>µrayé</td>
      <td></td>
      <td>Conso, pub</td>
      <td>non</td>
      <td>paper_mario</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Pikmin</td>
      <td>BE</td>
      <td>EM/BE</td>
      <td>TBE</td>
      <td>1 peu rayé</td>
      <td></td>
      <td>Conso, pub</td>
      <td>générique sans grattage</td>
      <td>pikmin</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Sonic heroes</td>
      <td>BE</td>
      <td>EM/BE (frottée, petit pli arrière bas)</td>
      <td>BE/TBE</td>
      <td>1 peu rayé</td>
      <td></td>
      <td></td>
      <td></td>
      <td>sonic_heroes</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Viewtiful Joe Red Hot Rumble</td>
      <td>TBE</td>
      <td>BE/TBE</td>
      <td>Nickel</td>
      <td>Nickel</td>
      <td></td>
      <td>Conso, pub</td>
      <td>oui</td>
      <td>viewtiful</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Waverace Blue Storm</td>
      <td>EM/BE</td>
      <td>EM (frottée)</td>
      <td>TBE</td>
      <td>µrayé</td>
      <td></td>
      <td>Pub</td>
      <td>non</td>
      <td>waverace</td>
      <td>x</td>
    </tr>
  </tbody>
</table>
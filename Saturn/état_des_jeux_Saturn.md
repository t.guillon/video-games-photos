## À propos

État général et contenu des jeux **Saturn**.

États:

 - ME : mauvais état
 - EM : état moyen
 - BE : bon état
 - TBE : très bon état
 - near mint : bah, near mint quoi

Le jugé des états reste subjectif, le mieux est d'aller voir les photos dans le dossier correspondant.

## Description

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: left;">
      <th>Jeu</th>
      <th>Général</th>
      <th>Boite</th>
      <th>Livret</th>
      <th>CD</th>
      <th>Cartouche</th>
      <th>Autre</th>
      <th>VIP</th>
      <th>Photo</th>
      <th>Pas à vendre</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Blazing Dragons (ALLEMAND!)</td>
      <td>TBE</td>
      <td>BE (du scotch arrière milieu gauche, et 1 peu frottée arrière haut à droite)</td>
      <td>Nickel</td>
      <td>µrayé</td>
      <td></td>
      <td></td>
      <td></td>
      <td>blazing_dragons</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Clockwork Knight</td>
      <td>EM/BE</td>
      <td>EM (en fait, BE mais il faut enlever des traces d'étiquette devant et une étiquette à l'arrière)</td>
      <td>TBE</td>
      <td>µrayé</td>
      <td></td>
      <td></td>
      <td></td>
      <td>clockwork_knight</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Crimewave</td>
      <td>BE</td>
      <td>EM/BE (quelques marques, 1 reste d'étiquette avec déchirure arrière haut)</td>
      <td>TBE</td>
      <td>µrayé</td>
      <td></td>
      <td></td>
      <td></td>
      <td>crimewave</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Marvel Super Heroes</td>
      <td>TBE/Near mint</td>
      <td>TBE (2/3 griffures sur l'avant)</td>
      <td>Nickel</td>
      <td>Nickel</td>
      <td></td>
      <td></td>
      <td></td>
      <td>marvel_super_heroes</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Robo Pit</td>
      <td>BE</td>
      <td>EM/BE (jaquette un peu gondolée avant/arrière, 1 petite déchirure recouverte de marqueur noir arrière en bas)</td>
      <td>Nickel</td>
      <td>µrayé</td>
      <td></td>
      <td></td>
      <td></td>
      <td>robo_pit</td>
      <td>x</td>
    </tr>
  </tbody>
</table>
## À propos

État général et contenu des jeux **PS3**.

États:

 - ME : mauvais état
 - EM : état moyen
 - BE : bon état
 - TBE : très bon état
 - near mint : bah, near mint quoi

Le jugé des états reste subjectif, le mieux est d'aller voir les photos dans le dossier correspondant.

## Description

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: left;">
      <th>Jeu</th>
      <th>Général</th>
      <th>Boite</th>
      <th>Livret</th>
      <th>CD</th>
      <th>Cartouche</th>
      <th>Autre</th>
      <th>VIP</th>
      <th>Photo</th>
      <th>Pas à vendre</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Boite vide</td>
      <td>TBE</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td>boite_vide</td>
      <td>x</td>
    </tr>
    <tr>
      <td>MW2 (NE PASSE PAS SUR MA PS3!)</td>
      <td>TBE</td>
      <td>TBE</td>
      <td>TBE</td>
      <td>1 seule rayure</td>
      <td></td>
      <td></td>
      <td></td>
      <td>mw3</td>
      <td>x</td>
    </tr>
  </tbody>
</table>
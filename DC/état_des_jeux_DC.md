## À propos

État général et contenu des jeux **DC**.

États:

 - ME : mauvais état
 - EM : état moyen
 - BE : bon état
 - TBE : très bon état
 - near mint : bah, near mint quoi

Le jugé des états reste subjectif, le mieux est d'aller voir les photos dans le dossier correspondant.

## Description

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: left;">
      <th>Jeu</th>
      <th>Général</th>
      <th>Boite</th>
      <th>Livret</th>
      <th>CD</th>
      <th>Cartouche</th>
      <th>Autre</th>
      <th>VIP</th>
      <th>Photo</th>
      <th>Pas à vendre</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Bangai-O</td>
      <td>TBE</td>
      <td>TBE</td>
      <td>nickel</td>
      <td>µrayé</td>
      <td></td>
      <td></td>
      <td></td>
      <td>bangaiO</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Evil Twin</td>
      <td>TBE</td>
      <td>TBE</td>
      <td>nickel</td>
      <td>nickel</td>
      <td></td>
      <td></td>
      <td></td>
      <td>evil_twin</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Fighting Force 2</td>
      <td>BE/TBE</td>
      <td>BE (1 fissure à l'arrière)</td>
      <td>nickel</td>
      <td>nickel</td>
      <td></td>
      <td>pub Eidos</td>
      <td></td>
      <td>fighting_force_2</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Floigan Bros</td>
      <td>BE/TBE</td>
      <td>BE (1 charnière  pétée)</td>
      <td>nickel</td>
      <td>nickel</td>
      <td></td>
      <td></td>
      <td></td>
      <td>floigan_bros</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Marvel VS Capcom</td>
      <td>BE</td>
      <td>BE (1 charnière pétée)</td>
      <td>BE (2/3 pliures sur devant)</td>
      <td>µrayé</td>
      <td></td>
      <td>tips pris dans un Dreamzone</td>
      <td></td>
      <td>marvel_vs_capcom</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Super Magnetic Neo</td>
      <td>BE</td>
      <td>EM (1 grosse fissure avant + 1 petite tranche en bas)</td>
      <td>TBE</td>
      <td>µrayé</td>
      <td></td>
      <td></td>
      <td></td>
      <td>super_magnetic_neo</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Sonic Shuffle</td>
      <td>BE/TBE</td>
      <td>BE (1 fissure + picots en moins)</td>
      <td>nickel</td>
      <td>quelques rayures</td>
      <td></td>
      <td></td>
      <td></td>
      <td>sonic_shuffle</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Toy Racer</td>
      <td>BE/TBE</td>
      <td>BE (1 charnière pétée + quelques fissures à l'avant et à l'arrière)</td>
      <td>nickel</td>
      <td>nickel</td>
      <td></td>
      <td></td>
      <td></td>
      <td>toy_racer</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Zombie Revenge</td>
      <td>Near mint</td>
      <td>TBE</td>
      <td>nickel</td>
      <td>TBE (1 très fine mais longue rayure à noter)</td>
      <td></td>
      <td></td>
      <td></td>
      <td>zombie_revenge</td>
      <td>x</td>
    </tr>
  </tbody>
</table>
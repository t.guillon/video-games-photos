
## À propos

Quelques jeux-vidéo à la vente, voir les photos et fichiers de description correspondants :

 - [DC](DC/état_des_jeux_DC.md)
 - [DS](DS/état_des_jeux_DS.md)
 - [GC](GC/état_des_jeux_GC.md)
 - [MD](MD/état_des_jeux_MD.md)
 - [N64](N64/état_des_jeux_N64.md)
 - [PS1](PS1/état_des_jeux_PS1.md)
 - [PS2](PS2/état_des_jeux_PS2.md)
 - [PS3](PS3/état_des_jeux_PS3.md)
 - [Saturn](Saturn/état_des_jeux_Saturn.md)
 - [SNIN](SNIN/état_des_jeux_SNIN.md)
 - [Switch](Switch/état_des_jeux_Switch.md)
 - [Wii](Wii/état_des_jeux_Wii.md)
 - [Wii\ U](Wii\ U/état_des_jeux_WiiU.md)

## Bonus

Un bout de code Python pour générer mes topics de vente [Gamopat](https://www.gamopat-forum.com) dans le dossier [scripting](scripting).

Y'a un objet pratique pour gérer le BBCode sans avoir à tout éditer à la main :
```python
>>> test = BBText('Salut', 'b', color='red')
>>> print(test)
[color=red][b]Salut[/b][/color]
```

Il est possible de mettre à jour le texte avec des attributs :
```python
>>> test2 = BBText('Salut')
>>> print(test2.bolded.colored('red'))
[color=red][b]Salut[/b][/color]
```

C'est pratique quand y'a beaucoup d'options à mettre :
```python
>>> test3 = BBText(
    'Welcome to this GitLab project!',
    b=None,
    i=None,
    u=None,
    size=12,
    color='#0000ff',
    url='https://gitlab.com/t.guillon/video-games-photos',
    spoiler='Caché'
    )
>>> test3
BBText("Welcome to this GitLab project!", b=None, i=None, u=None, size=12, color="#0000ff", url="https://gitlab.com/t.guillon/video-games-photos", spoiler="Caché")
>>> str(test3)
'[spoiler=Caché][url=https://gitlab.com/t.guillon/video-games-photos][color=#0000ff][size=12][u][i][b]Welcome to this GitLab project![/b][/i][/u][/size][/color][/url][/spoiler]'

```

## Licence

Pas de licence particulière, pensez juste à faire une citation!

À plus


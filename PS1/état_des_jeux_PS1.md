## À propos

État général et contenu des jeux **PS1**.

États:

 - ME : mauvais état
 - EM : état moyen
 - BE : bon état
 - TBE : très bon état
 - near mint : bah, near mint quoi

Le jugé des états reste subjectif, le mieux est d'aller voir les photos dans le dossier correspondant.

## Description

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: left;">
      <th>Jeu</th>
      <th>Général</th>
      <th>Boite</th>
      <th>Livret</th>
      <th>CD</th>
      <th>Cartouche</th>
      <th>Autre</th>
      <th>VIP</th>
      <th>Photo</th>
      <th>Pas à vendre</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Dino Crisis</td>
      <td>BE</td>
      <td>EM/BE</td>
      <td>TBE</td>
      <td>µrayé</td>
      <td></td>
      <td></td>
      <td></td>
      <td>dino_crisis</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Heart Of Darkness</td>
      <td>TBE</td>
      <td>BE/TBE</td>
      <td>TBE</td>
      <td>µrayé (2CDs)</td>
      <td></td>
      <td>lunettes 3D EM/BE</td>
      <td></td>
      <td>heart_of_darkness</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Largo Winch</td>
      <td>TBE</td>
      <td>TBE</td>
      <td></td>
      <td>µrayé</td>
      <td></td>
      <td></td>
      <td></td>
      <td>largo_winch</td>
      <td></td>
    </tr>
    <tr>
      <td>L'Odyssée d'Abe</td>
      <td>TBE</td>
      <td>BE (1 fissure sur la tranche)</td>
      <td>TBE</td>
      <td>µrayé</td>
      <td></td>
      <td></td>
      <td></td>
      <td>oddworld</td>
      <td></td>
    </tr>
    <tr>
      <td>L'Exode d'Abe</td>
      <td>Near mint</td>
      <td>TBE</td>
      <td>nickel</td>
      <td>TBE (2 CDs)</td>
      <td></td>
      <td>courrier retour, règles rapides</td>
      <td></td>
      <td>oddworld_exode</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Parappa The Rapper</td>
      <td>TBE (near mint sauf la fissure)</td>
      <td>TBE (sauf 1 fissure)</td>
      <td>TBE</td>
      <td>nickel</td>
      <td></td>
      <td></td>
      <td></td>
      <td>parappa</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Tail Concerto</td>
      <td>TBE</td>
      <td>BE/TBE</td>
      <td>TBE</td>
      <td>quelques rayures</td>
      <td></td>
      <td></td>
      <td></td>
      <td>tail_concerto</td>
      <td>x</td>
    </tr>
  </tbody>
</table>
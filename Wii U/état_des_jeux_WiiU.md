## À propos

État général et contenu des jeux **Wii U**.

États:

 - ME : mauvais état
 - EM : état moyen
 - BE : bon état
 - TBE : très bon état
 - near mint : bah, near mint quoi

Le jugé des états reste subjectif, le mieux est d'aller voir les photos dans le dossier correspondant.

## Description

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: left;">
      <th>Jeu</th>
      <th>Général</th>
      <th>Boite</th>
      <th>Livret</th>
      <th>CD</th>
      <th>Cartouche</th>
      <th>Autre</th>
      <th>VIP</th>
      <th>Photo</th>
      <th>Pas à vendre</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Bayonetta 1 &amp; 2 Special Edition</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td>Carton nickel</td>
      <td></td>
      <td>bayonetta</td>
      <td>x</td>
    </tr>
    <tr>
      <td>- Bayonetta 1</td>
      <td>BE/TBE</td>
      <td>TBE (sauf gondolage avant bas et arrière haut)</td>
      <td>Nickel</td>
      <td>Nickel</td>
      <td></td>
      <td>Infos, manuel, pub</td>
      <td>oui</td>
      <td></td>
      <td>x</td>
    </tr>
    <tr>
      <td>- Bayonetta 2</td>
      <td>BE/TBE</td>
      <td>TBE (sauf pliure arrière bas)</td>
      <td>Nickel</td>
      <td>Nickel</td>
      <td></td>
      <td>Infos, manuel, pub</td>
      <td>oui</td>
      <td></td>
      <td>x</td>
    </tr>
    <tr>
      <td>Ninja Gaiden</td>
      <td>TBE</td>
      <td>TBE</td>
      <td>Nickel</td>
      <td>Nickel</td>
      <td></td>
      <td>Infos, guide rapide</td>
      <td>oui</td>
      <td>ninja_gaiden_3</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Skylanders Superchargers</td>
      <td>TBE</td>
      <td>TBE</td>
      <td>Nickel</td>
      <td>Nickel</td>
      <td></td>
      <td>Infos, guide rapide</td>
      <td>non</td>
      <td>skylanders_superchargers</td>
      <td>x</td>
    </tr>
  </tbody>
</table>
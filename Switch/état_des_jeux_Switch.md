## À propos

État général et contenu des jeux **Switch**.

États:

 - ME : mauvais état
 - EM : état moyen
 - BE : bon état
 - TBE : très bon état
 - near mint : bah, near mint quoi

Le jugé des états reste subjectif, le mieux est d'aller voir les photos dans le dossier correspondant.

## Description

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: left;">
      <th>Jeu</th>
      <th>Général</th>
      <th>Boite</th>
      <th>Livret</th>
      <th>CD</th>
      <th>Cartouche</th>
      <th>Autre</th>
      <th>VIP</th>
      <th>Photo</th>
      <th>Pas à vendre</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Dicey Dungeons SRG</td>
      <td>Neuf blister</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td>dicey</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Grim Fandango iam8bit</td>
      <td>Neuf blister</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td>Fourreau cartonné</td>
      <td></td>
      <td>grim</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Hadès</td>
      <td>TBE</td>
      <td>TBE (quelques traces mais discrètes, les photos les font ressortir à bloc)</td>
      <td>TBE</td>
      <td></td>
      <td>TBE</td>
      <td></td>
      <td></td>
      <td>hades</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Manette pro</td>
      <td>Near mint</td>
      <td>TBE</td>
      <td>TBE</td>
      <td></td>
      <td></td>
      <td>Livret</td>
      <td></td>
      <td>manette_pro</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Manifold Garden iam8bit</td>
      <td>Neuf blister</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td>manifold</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Manifold Garden JAP</td>
      <td>Neuf blister</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td>CD BO, carte postale, rouleau adhésif avec motif</td>
      <td></td>
      <td>manifold_jp</td>
      <td></td>
    </tr>
    <tr>
      <td>Mutazione iam8bit</td>
      <td>Neuf blister</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td>mutazione</td>
      <td></td>
    </tr>
    <tr>
      <td>Windjammers LRG</td>
      <td>Near mint</td>
      <td>TBE</td>
      <td>TBE</td>
      <td></td>
      <td>TBE</td>
      <td>Livret (blibli), carte, couv réversible, seconde boîte TBE</td>
      <td></td>
      <td>windjammers</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Windjammers PixNLove</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td>Carton nickel</td>
      <td></td>
      <td>windjammers_pix</td>
      <td>x</td>
    </tr>
    <tr>
      <td>- Windjammers 1</td>
      <td>Neuf blister</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>- Windjammers 2</td>
      <td>Neuf blister</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>Steelbook Super Mario 3D World (PAS DE JEU)</td>
      <td>Near mint</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td>smw3dw_steel_book</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Super Mario Bros U Deluxe</td>
      <td>Near mint</td>
      <td>TBE</td>
      <td></td>
      <td></td>
      <td>TBE</td>
      <td>Avertissement TBE</td>
      <td></td>
      <td>smb_u_deluxe</td>
      <td>x</td>
    </tr>
  </tbody>
</table>
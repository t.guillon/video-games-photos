## À propos

État général et contenu des jeux **SNIN**.

États:

 - ME : mauvais état
 - EM : état moyen
 - BE : bon état
 - TBE : très bon état
 - near mint : bah, near mint quoi

Le jugé des états reste subjectif, le mieux est d'aller voir les photos dans le dossier correspondant.

## Description

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: left;">
      <th>Jeu</th>
      <th>Général</th>
      <th>Boite</th>
      <th>Livret</th>
      <th>CD</th>
      <th>Cartouche</th>
      <th>Autre</th>
      <th>VIP</th>
      <th>Photo</th>
      <th>Pas à vendre</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Aladdin</td>
      <td>TBE</td>
      <td></td>
      <td></td>
      <td></td>
      <td>FRA</td>
      <td>Mini repro nickel</td>
      <td></td>
      <td>aladdin</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Alien 3</td>
      <td>BE</td>
      <td></td>
      <td></td>
      <td></td>
      <td>FAH, arrière un peu jauni</td>
      <td>Mini repro nickel</td>
      <td></td>
      <td>alien</td>
      <td>x</td>
    </tr>
    <tr>
      <td>The Chaos Engine</td>
      <td>TBE</td>
      <td></td>
      <td></td>
      <td></td>
      <td>FAH, arrière un peu jauni</td>
      <td>Mini repro nickel</td>
      <td></td>
      <td>chaos_engine</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Doom</td>
      <td>TBE</td>
      <td></td>
      <td></td>
      <td></td>
      <td>FAH</td>
      <td></td>
      <td></td>
      <td>doom</td>
      <td>x</td>
    </tr>
    <tr>
      <td>DBZ 1</td>
      <td>BE</td>
      <td></td>
      <td></td>
      <td></td>
      <td>FRA, un peu jaunie, étiquette sunfade</td>
      <td>Mini repro nickel</td>
      <td></td>
      <td>dragon_ball_z_1</td>
      <td>x</td>
    </tr>
    <tr>
      <td>DBZ 2</td>
      <td>BE</td>
      <td></td>
      <td></td>
      <td></td>
      <td>FRA, arrière jauni</td>
      <td>Mini repro nickel</td>
      <td></td>
      <td>dragon_ball_z_2</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Mr. Nutz</td>
      <td>TBE</td>
      <td></td>
      <td></td>
      <td></td>
      <td>FAH, arrière un peu jauni</td>
      <td>Mini repro nickel</td>
      <td></td>
      <td>mr_nutz</td>
      <td>x</td>
    </tr>
    <tr>
      <td>NBA Jam</td>
      <td>BE</td>
      <td></td>
      <td></td>
      <td></td>
      <td>FAH, arrière jauni</td>
      <td>Mini repro nickel</td>
      <td></td>
      <td>nba_jam</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Starwing</td>
      <td>TBE</td>
      <td></td>
      <td></td>
      <td></td>
      <td>FAH, arrière un peu jauni</td>
      <td>Mini repro TBE</td>
      <td></td>
      <td>starfox</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Street Racer</td>
      <td>TBE</td>
      <td></td>
      <td></td>
      <td></td>
      <td>EUR, 1 légère trace à l'avant</td>
      <td></td>
      <td></td>
      <td>street_racer</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Super Mario World</td>
      <td>TBE</td>
      <td></td>
      <td></td>
      <td></td>
      <td>FAH, arrière un peu jauni</td>
      <td></td>
      <td></td>
      <td>smw1</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Super Mario World 2 - Yoshi's Island</td>
      <td>TBE</td>
      <td></td>
      <td></td>
      <td></td>
      <td>FAH</td>
      <td>Mini repro nickel</td>
      <td></td>
      <td>smw2</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Super Pang</td>
      <td>BE</td>
      <td></td>
      <td></td>
      <td></td>
      <td>FAH, arrière jauni</td>
      <td></td>
      <td></td>
      <td>super_pang</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Super Street Fighter 2</td>
      <td>BE</td>
      <td></td>
      <td></td>
      <td></td>
      <td>FAH, coin supérieur droit étiquette corné, quelques traces de feutre sur l'arrière</td>
      <td>Mini repro nickel</td>
      <td></td>
      <td>super_street_fighter_2</td>
      <td>x</td>
    </tr>
    <tr>
      <td>True Lies</td>
      <td>BE</td>
      <td></td>
      <td></td>
      <td></td>
      <td>EUR, coin inférieur droit un peu corné, arrière un peu jauni</td>
      <td>Mini repro nickel</td>
      <td></td>
      <td>true_lies</td>
      <td>x</td>
    </tr>
  </tbody>
</table>
""" Some practical objects for Gamopat topics """
from dataclasses import dataclass, field
import re
from typing import Union

CLOSING_BBCODE = re.compile('\[\/.*?\]')
HTML_SPACE = '%20'
WARNING = 'https://2img.net/i/fa/twemoji/16x16/26a0.png'
CAMERA = 'https://illiweb.com/fa/twemoji/16x16/1f4f7.png'

class BBText:
    """ A class for dealing with a single string enhanced with BBCode """

    def __init__(self, text, *args, **kwargs):
        """ Setting text, and optional BBCode properties
        
        args will be interpreted as BBCode codes with no values
        (key,val) in kwargs are BBCode codes (key) and possible value (leave
        None if no specific value)
        
        See https://www.bbcode.org/reference.php for available codes
        
        """
        self.text = str(text)
        self.properties={}
        for arg in args:
            self.properties[arg] = None
        self.properties.update(kwargs)
        
    def code_converter(self, code):
        """ Converting some codes from more intuitive names """
        conversions = dict(
            bold='b',
            italic='i',
            s='strike',
            underline='u'
            )
        return conversions.get(code, code)
        
    def add_code(self, *args, **kwargs):
        """ Adding codes without (args) or  with (kwargs) values """
        codes = dict((arg, None) for arg in args)
        codes.update(kwargs)
        for code, value in codes.items():
            code = self.code_converter(code)
            self.properties[code] = value
        
    def remove_code(self, *args):
        """ Deleting any code in args """
        for code in args:
            code = self.code_converter(code)
            self.properties.pop(code)
        
    def toggle_bold(self):
        """ Turning b code on/off """
        if 'b' in self.properties:
            self.remove_code('b')
        else:
            self.add_code('b')
        
    def apply_code(self):
        """ Enhance class text with all BBCode codes """
        text = self.text
        for code, value in self.properties.items():
            val_ok = value not in (None, '')
            start = f'[{code}={value}]' if val_ok else f'[{code}]'
            end = f'[/{code}]'
            text = f'{start}{text}{end}'
        return text
        
    def copy(self):
        """ A deep copy of entity """
        return BBText(self.text, **self.properties)
        
    def updated_copy(self, code, value=None):
        """ Deep copy with additional code (and optionnal value) """
        cp = self.copy()
        kwargs = {code: value}
        cp.add_code(**kwargs)
        return cp
        
    @property
    def bolded(self):
        """ b code """
        return self.updated_copy('b')
        
    @property
    def italicized(self):
        """ i code """
        return self.updated_copy('i')
        
    @property
    def striked(self):
        """ strike code """
        return self.updated_copy('s')
        
    @property
    def underlined(self):
        """ u code """
        return self.updated_copy('u')
        
    @property
    def spoilered(self):
        """ spoiler code """
        return self.updated_copy('spoiler')
        
    @property
    def urled(self):
        """ url code """
        cp = self.updated_copy('url')
        cp.text = cp.text.replace(' ', HTML_SPACE)
        return cp
        
    def colored(self, color):
        """ color code with color color """
        return self.updated_copy('color', color)
        
    def sized(self, size):
        """ size code with size size """
        return self.updated_copy('size', size)
            
    def imged(self, width=None, height=None):
        """ img code with optional dimensions """
        if width and height:
            dim = f'{width}x{height}'
        else:
            dim = None
        return self.updated_copy('img', dim)
        
    def hyperlinked(self, url):
        """ url code pointing to url """
        return self.updated_copy('url', url.replace(' ', HTML_SPACE))
        
    def spoiling(self, spoil):
        """ spoiler code with spoil as title """
        return self.updated_copy('spoiler', spoil)
        
    def __getitem__(self, key):
        return self.properties.get(key)
        
    def __repr__(self):
        args = [f'"{self.text}"'] + [
            f'{k}={v}'
            if not isinstance(v, str)
            else f'{k}="{v}"'
            for k,v in self.properties.items()
            ]
        rep = f'{type(self).__name__}({", ".join(args)})'
        return rep
        
    def __str__(self):
        return self.apply_code()

WARNING = BBText(WARNING).imged()
CAMERA = BBText(CAMERA).imged()

@dataclass
class GameDescription:
    """ A class for representing games on topic """
    name: str = None
    machine: str = None
    warning: str = None
    state_overall: str = None
    state_box: str = None
    state_booklet: str = None
    state_cd: str = None
    state_cartridge: str = None
    other_items: str = None
    vip_card: str = None
    photos: Union[str,list,tuple] = None
    sold_to: Union[str,bool] = None
    booked_by: Union[str,bool] = None
    prices: Union[int,float,list,tuple] = None
    
    def __post_init__(self):
        if self.prices is not None:
            if isinstance(self.prices, (int, float, str)):
                self.prices = [self.prices]
            self.prices = [
                int(price)
                if isinstance(price, float) and price.is_integer()
                else price
                for price in self.prices
                ]
        if self.photos is not None:
            if isinstance(self.photos, str):
                self.photos = [self.photos]
    
    @property
    def loose(self):
        """ Whether game is a loose cartridge """
        return (
            self.state_cartridge is not None
            and self.state_box is None
            and self.state_booklet is None
            )
            
    @property
    def states(self):
        """ Getting all states """
        states = dict(
            (k, getattr(self, f'state_{k}'))
            for k in ('overall', 'box', 'booklet', 'cd', 'cartridge')
            if getattr(self, f'state_{k}') is not None
            )
        if states:
            return states
            
    @property
    def others(self):
        """ Getting VIP and other items """
        others = dict()
        if self.vip_card is not None:
            others['VIP'] = self.vip_card
        if self.other_items is not None:
            others['Misc.'] = self.other_items
        if others:
            return others
            
    @property
    def booked(self):
        """ Whether game is booked by someone """
        return self.booked_by is not None
        
    @property
    def booker(self):
        """ The game booker """
        if isinstance(self.booked_by, bool):
            return ''
        elif self.booked_by is not None:
            return self.booked_by  
            
    @property
    def sold(self):
        """ Whether game was sold """
        return self.sold_to is not None
        
    @property
    def buyer(self):
        """ The game buyer """
        if isinstance(self.sold_to, bool):
            return ''
        elif self.sold_to is not None:
            return self.sold_to    
    
    @property
    def name_repr(self):
        """ Name """
        name = BBText(self.name).bolded
        if self.loose:
            name = f'{name} (loose)'
        else:
            name = str(name)
        return name
        
    @property
    def warning_repr(self):
        """ Warning message """
        if self.warning is None:
            return
        warning = BBText(self.warning).bolded.colored('red')
        return f'{WARNING} {warning} {WARNING}'
        
    @property
    def prices_repr(self):
        """ Prices """
        if ((self.prices is None)
            or (hasattr(self.prices, '__len__') and len(self.prices)==0)
            ):
            return
        revised = len(self.prices) > 1
        is_gift = self.prices[0]==0
        if is_gift:
            return BBText('don').bolded.colored('blue')
        prices = [BBText(price).colored('blue') for price in self.prices]
        prices[:-1] = [price.striked for price in prices[:-1]]
        prices[-1].add_code('bold')
        prices.append(BBText('out', **prices[-1].properties))
        return ' '.join(str(price) for price in prices)
    
    @property
    def head_repr(self):
        """ Game header (name+price+warning+sold|booked) """
        head = self.name_repr
        warning = self.warning_repr
        prices = self.prices_repr
        booked = self.booked_repr
        if warning:
            head = f'{head} {warning}'
        if prices:
            head = f'{head} {prices}'
        if self.sold:
            head = self._sold_repr(head)
        elif self.booked:
            head = f'{head} {booked}'
        return head
        
    @property
    def state_repr(self):
        """ State of game content """
        if self.states is None:
            return
        if not self.loose:
            states = [str(BBText(self.state_overall))]
            items = dict(
                Boite = self.state_box,
                Livret = self.state_booklet,
                CD = self.state_cd,
                Cartouche = self.state_cartridge
                )
            states.extend([
                f'{name}: {BBText(item)}'
                for name, item in items.items()
                if item is not None
                ])
        else:
            states = [
                f'{BBText(self.state_overall)} : {BBText(self.state_cartridge)}'
                ]
        return '\n - '.join(states)
        
    @property
    def other_repr(self):
        """ VIP and additional info """
        if self.others is None:
            return
        others = []
        if self.vip_card is not None:
            others.append(f' - VIP: {BBText(self.vip_card)}')
        if self.other_items is not None:
            others.append(f' - Autre: {BBText(self.other_items)}')
        return '\n'.join(other for other in others)
    
    @property
    def photos_repr(self):
        """ Photos """
        if self.photos is None:
            return
        photos = '\n'.join([
            str(BBText(photo, img=None, url=photo))
            for photo in self.photos
            ])
        return str(BBText(photos).spoiling('Photos'))
        
    @property
    def booked_repr(self):
        """ Booked """
        return str(BBText(f'réservé {self.booker}').underlined)
        
    def _sold_repr(self, line=''):
        """ Add a mark for sold games """
        kind = 'vendu'
        is_gift = self.prices[0]==0
        if is_gift:
            kind = 'donné'
        return f'{BBText(line).striked} {kind} {self.buyer}'
        
    def __str__(self):
        description = [
            self.head_repr,
            self.state_repr,
            self.other_repr,
            self.photos_repr
            ]
        return '\n'.join([descr for descr in description if descr is not None])
        
@dataclass
class BundleDescription:
    """ A class for representing game bundles on topic """
    games: Union[list,tuple] = None
    name: str = None
    warning: str = None
    state: str = None
    photos: Union[str,list,tuple] = None
    sold_to: Union[str,bool] = None
    booked_by: Union[str,bool] = None
    prices: Union[int,float,list,tuple] = None
    
    def __post_init__(self):
        if self.prices is not None:
            if isinstance(self.prices, (int, float)):
                self.prices = [self.prices]
            self.prices = [
                int(price)
                if isinstance(price, float) and price.is_integer()
                else price
                for price in self.prices
                ]
        if self.photos is not None:
            if isinstance(self.photos, str):
                self.photos = [self.photos]
            
    @property
    def booked(self):
        """ Whether game is booked by someone """
        return self.booked_by is not None
        
    @property
    def booker(self):
        """ The game booker """
        if isinstance(self.booked_by, bool):
            return ''
        elif self.booked_by is not None:
            return self.booked_by  
            
    @property
    def sold(self):
        """ Whether game was sold """
        return self.sold_to is not None
        
    @property
    def buyer(self):
        """ The game buyer """
        if isinstance(self.sold_to, bool):
            return ''
        elif self.sold_to is not None:
            return self.sold_to  
    
    @property
    def name_repr(self):
        """ Name """
        return str(BBText(self.name).bolded)
        
    @property
    def state_repr(self):
        """ Name """
        if self.state is None:
            return
        return str(BBText(self.state))
        
    @property
    def warning_repr(self):
        """ Warning message """
        if self.warning is None:
            return
        warning = BBText(self.warning).bolded.colored('red')
        return f'{WARNING} {warning} {WARNING}'
        
    @property
    def prices_repr(self):
        """ Prices """
        if self.prices is None:
            return
        revised = len(self.prices) > 1
        is_gift = self.prices[0]==0
        if is_gift:
            return BBText('don').bolded.colored('blue')
        prices = [BBText(price).colored('blue') for price in self.prices]
        prices[:-1] = [price.striked for price in prices[:-1]]
        prices[-1].add_code('bold')
        prices.append(BBText('out', **prices[-1].properties))
        return ' '.join(str(price) for price in prices)
    
    @property
    def head_repr(self):
        """ Game header (name+price+warning) """
        head = self.name_repr
        warning = self.warning_repr
        prices = self.prices_repr
        booked = self.booked_repr
        if warning:
            head = f'{head} {warning}'
        if prices:
            head = f'{head} {prices}'
        if self.sold:
            head = self._sold_repr(head)
        elif self.booked:
            head = f'{head} {booked}'
        return head
        
    @property
    def games_repr(self):
        """ Games content """
        games = []
        for game in self.games:
            description = [
                descr
                for descr in (game.name_repr, game.state_repr, game.other_repr)
                if descr is not None
                ]
            games.append('\n'.join(description))
        return '\n'.join(games)
    
    @property
    def photos_repr(self):
        """ Photos """
        if self.photos is None:
            return ''
        photos = '\n'.join([
            str(BBText(photo, img=None, url=photo))
            for photo in self.photos
            ])
        return str(BBText(photos).spoiling('Photos'))
        
    @property
    def booked_repr(self):
        """ Booked """
        return str(BBText(f'réservé {self.booker}').underlined)
        
    def _sold_repr(self, line=''):
        """ Add a mark for sold bundles """
        kind = 'vendu'
        is_gift = self.prices[0]==0
        if is_gift:
            kind = 'donné'
        return f'{BBText(line).striked} {kind} {self.buyer}'
        
    def __str__(self):
        description = [
            self.head_repr,
            self.state_repr,
            self.games_repr,
            self.photos_repr
            ]
        return '\n'.join([descr for descr in description if descr is not None])

def python_excerpt(code):
    """ Python code HTML-formatted as code excerpt
    
    Note
    ----
    Does not seem to work properly since bbcode inside the excerpt is executed
    anyway on Gamopat... Might be a better idea to create examples in the Readme
    
    """
    if isinstance(code, (list, tuple)):
        code = '\n'.join(code)
    code = code.replace('[', '[ ').replace(']', ' ]') # Avoid BBCode execution
    code = code.replace('\n','<br>') # For fine printing of newlines on topic
    return (
        f'<dl class="codebox"><dt>Python:</dt><dd><code>{code}</code></dd></dl>'
        )
        
if __name__ == '__main__':
    test = BBText('Salut la compagnie', b=None, size=18)
    test.imged()
    test.imged(100,50)
    test2 = BBText('Wesh')

    test2mod = test2.bolded.colored('blue').sized(10)
    print(test2mod)
    link = 'https://gitlab.com/t.guillon/video-games-photos/-/raw/master/GC/battalion_wars_1.jpg'
    print(test2.hyperlinked(link).spoiling('Wolala'))

    code = [
    ">>> test = BBText('Salut la compagnie', b=None, size=18)",
    ">>> print(test)",
    "[size=18][b]Salut la compagnie[/b][/size]"
    ]
    print(python_excerpt(code))
""" Creating price estimation topics """
import pathlib
import sys

HEADDIR = pathlib.Path(__file__).parents[1]
SOURCEDIR = HEADDIR.parent

sys.path.append(str(HEADDIR))
import load_database
import make_topic as mt

database = SOURCEDIR/'description.xlsx'
gifts = [
    'Boite vide',
    'Gaine Wiimote noire',
    'Largo Winch',
    'MW2',
    'Skylanders Superchargers',
    'Trackmania Turbo'
    ]
    
games = load_database.extract_database(database)
games = games[~games['name'].isin(gifts)]
content = load_database.get_games_description(games)

machines_per_brand = dict(
    Nintendo=['DS', 'N64', 'GC', 'SNIN', 'Switch', 'Wii', 'Wii U'],
    Sega=['DC', 'MD', 'Saturn'],
    Sony=['PS1', 'PS2', 'PS3']
    )
topics_urls = dict(
    Nintendo='https://www.gamopat-forum.com/t116906-estim-du-nintendo',
    Sega='https://www.gamopat-forum.com/t120310-estim-du-sega',
    Sony='https://www.gamopat-forum.com/t116907-estim-du-sony',
    )
brands = dict(
    (machine, brand)
    for brand, machines in machines_per_brand.items()
    for machine in machines
    )

games_per_brand = {}
for machine, items in content.items():
    brand = games_per_brand.setdefault(brands[machine], [])
    if items:
        section = [
            str(mt.BBText(machine).bolded.sized(24)),
            '\n'.join(str(item) for item in items),
            ''
            ]
        brand.extend(section)
        
inners_per_brand = dict(
    (brand, '\n'.join(inner))
    for brand,inner in games_per_brand.items()
    )

header_template = """
Salut tout le monde!

En prévision d'une vente ici même, pourriez-vous svp me filer un coup de pouce pour estimer les jeux {brand} suivants?
(y'a pas mal de photos, le topic peut potentiellement mettre une blinde à charger)

J'ai aussi des jeux {brand_others} à estimer svp!

"""

footer = """Encore merci, les jeunes, et à bientôt!"""

brand_others = dict(
    (   brand,
        [   str(mt.BBText(brand_other, url=topics_urls[brand_other]))
            for brand_other in machines_per_brand
            if brand_other != brand
            ]
        )
    for brand in machines_per_brand
    )
brand_others = dict(
    (brand, f"{', '.join(others[:-1])} et {others[-1]}")
    for brand, others in brand_others.items()
    )

topics = {}
for brand, inner in inners_per_brand.items():
    topic = [
        header_template.format(brand=brand, brand_others=brand_others[brand]),
        inner,
        footer
        ]
    topics[brand] = topic

# Exporting
for brand, topic in topics.items():
    with open(f'estim_{brand}.txt', 'w') as wt:
        wt.write('\n'.join(topic))

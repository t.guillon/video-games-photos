
Salut tout le monde!

En pr�vision d'une vente ici m�me, pourriez-vous svp me filer un coup de pouce pour estimer les jeux Nintendo suivants?
(y'a pas mal de photos, le topic peut potentiellement mettre une blinde � charger)

J'ai aussi des jeux [url=https://www.gamopat-forum.com/t120310-estim-du-sega]Sega[/url] et [url=https://www.gamopat-forum.com/t116907-estim-du-sony]Sony[/url] � estimer svp!


[size=24][b]GC[/b][/size]
[b]Battalion Wars[/b]
BE/TBE
 - Boite: BE (1 peu frott�e)
 - Livret: TBE
 - CD: �ray�
 - VIP: non gratt�s
 - Autre: Pub
[spoiler=Photos][url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/GC/battalion_wars_1.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/GC/battalion_wars_1.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/GC/battalion_wars_2.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/GC/battalion_wars_2.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/GC/battalion_wars_3.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/GC/battalion_wars_3.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/GC/battalion_wars_4.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/GC/battalion_wars_4.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/GC/battalion_wars_5.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/GC/battalion_wars_5.jpg[/img][/url][/spoiler]
[b]Paper Mario[/b]
TBE
 - Boite: TBE
 - Livret: Nickel
 - CD: �ray�
 - VIP: absents
 - Autre: Conso, pub
[spoiler=Photos][url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/GC/paper_mario_1.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/GC/paper_mario_1.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/GC/paper_mario_2.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/GC/paper_mario_2.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/GC/paper_mario_3.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/GC/paper_mario_3.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/GC/paper_mario_4.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/GC/paper_mario_4.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/GC/paper_mario_5.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/GC/paper_mario_5.jpg[/img][/url][/spoiler]
[b]Sonic heroes[/b]
BE
 - Boite: EM/BE (frott�e, petit pli arri�re bas)
 - Livret: BE/TBE
 - CD: 1 peu ray�
[spoiler=Photos][url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/GC/sonic_heroes_1.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/GC/sonic_heroes_1.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/GC/sonic_heroes_2.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/GC/sonic_heroes_2.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/GC/sonic_heroes_3.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/GC/sonic_heroes_3.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/GC/sonic_heroes_4.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/GC/sonic_heroes_4.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/GC/sonic_heroes_5.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/GC/sonic_heroes_5.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/GC/sonic_heroes_6.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/GC/sonic_heroes_6.jpg[/img][/url][/spoiler]
[b]Waverace Blue Storm[/b]
EM/BE
 - Boite: EM (frott�e)
 - Livret: TBE
 - CD: �ray�
 - VIP: absents
 - Autre: Pub
[spoiler=Photos][url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/GC/waverace_1.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/GC/waverace_1.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/GC/waverace_2.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/GC/waverace_2.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/GC/waverace_3.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/GC/waverace_3.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/GC/waverace_4.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/GC/waverace_4.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/GC/waverace_5.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/GC/waverace_5.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/GC/waverace_6.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/GC/waverace_6.jpg[/img][/url][/spoiler]

[size=24][b]N64[/b][/size]
[b]Banjo Kazooie[/b] (loose)
TBE
[spoiler=Photos][url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/N64/banjo_kazooie.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/N64/banjo_kazooie.jpg[/img][/url][/spoiler]
[b]F Zero X[/b] (loose)
TBE
[spoiler=Photos][url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/N64/f_zero_x.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/N64/f_zero_x.jpg[/img][/url][/spoiler]
[b]Lylat Wars[/b] (loose)
TBE
[spoiler=Photos][url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/N64/lylat_wars.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/N64/lylat_wars.jpg[/img][/url][/spoiler]
[b]Mario Kart[/b] (loose)
TBE
[spoiler=Photos][url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/N64/mario_kart.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/N64/mario_kart.jpg[/img][/url][/spoiler]
[b]Silicon valley[/b] (loose)
TBE
[spoiler=Photos][url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/N64/space_station_silicon_valley.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/N64/space_station_silicon_valley.jpg[/img][/url][/spoiler]
[b]Rogue Squadron[/b] (loose)
TBE
[spoiler=Photos][url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/N64/star_wars_rogue_squadron.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/N64/star_wars_rogue_squadron.jpg[/img][/url][/spoiler]
[b]Yoshi's Story[/b] (loose)
TBE
[spoiler=Photos][url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/N64/yoshi.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/N64/yoshi.jpg[/img][/url][/spoiler]

[size=24][b]SNIN[/b][/size]
[b]Aladdin[/b] (loose)
TBE
[spoiler=Photos][url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/aladdin_1.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/aladdin_1.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/aladdin_2.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/aladdin_2.jpg[/img][/url][/spoiler]
[b]Alien 3[/b] (loose)
BE
[spoiler=Photos][url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/alien_1.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/alien_1.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/alien_2.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/alien_2.jpg[/img][/url][/spoiler]
[b]The Chaos Engine[/b] (loose)
TBE
[spoiler=Photos][url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/chaos_engine_1.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/chaos_engine_1.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/chaos_engine_2.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/chaos_engine_2.jpg[/img][/url][/spoiler]
[b]Doom[/b] (loose)
TBE
[spoiler=Photos][url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/doom_1.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/doom_1.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/doom_2.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/doom_2.jpg[/img][/url][/spoiler]
[b]DBZ 1[/b] (loose)
BE
[spoiler=Photos][url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/dragon_ball_z_1_1.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/dragon_ball_z_1_1.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/dragon_ball_z_1_2.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/dragon_ball_z_1_2.jpg[/img][/url][/spoiler]
[b]DBZ 2[/b] (loose)
BE
[spoiler=Photos][url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/dragon_ball_z_2_1.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/dragon_ball_z_2_1.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/dragon_ball_z_2_2.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/dragon_ball_z_2_2.jpg[/img][/url][/spoiler]
[b]Mr. Nutz[/b] (loose)
TBE
[spoiler=Photos][url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/mr_nutz_1.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/mr_nutz_1.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/mr_nutz_2.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/mr_nutz_2.jpg[/img][/url][/spoiler]
[b]NBA Jam[/b] (loose)
BE
[spoiler=Photos][url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/nba_jam_1.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/nba_jam_1.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/nba_jam_2.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/nba_jam_2.jpg[/img][/url][/spoiler]
[b]Starwing[/b] (loose)
TBE
[spoiler=Photos][url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/smw1_1.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/smw1_1.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/smw1_2.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/smw1_2.jpg[/img][/url][/spoiler]
[b]Street Racer[/b] (loose)
TBE
[spoiler=Photos][url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/smw2_1.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/smw2_1.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/smw2_2.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/smw2_2.jpg[/img][/url][/spoiler]
[b]Super Mario World[/b] (loose)
TBE
[spoiler=Photos][url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/starfox_1.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/starfox_1.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/starfox_2.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/starfox_2.jpg[/img][/url][/spoiler]
[b]Super Mario World 2 - Yosh'is Island[/b] (loose)
TBE
[spoiler=Photos][url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/street_racer_1.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/street_racer_1.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/street_racer_2.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/street_racer_2.jpg[/img][/url][/spoiler]
[b]Super Pang[/b] (loose)
BE
[spoiler=Photos][url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/super_pang_1.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/super_pang_1.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/super_pang_2.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/super_pang_2.jpg[/img][/url][/spoiler]
[b]Super Street Fighter 2[/b] (loose)
BE
[spoiler=Photos][url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/super_street_fighter_2_1.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/super_street_fighter_2_1.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/super_street_fighter_2_2.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/super_street_fighter_2_2.jpg[/img][/url][/spoiler]
[b]True Lies[/b] (loose)
BE
[spoiler=Photos][url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/true_lies_1.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/true_lies_1.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/true_lies_2.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/SNIN/true_lies_2.jpg[/img][/url][/spoiler]

[size=24][b]Switch[/b][/size]
[b]Windjammers LRG[/b]
Near mint
 - Boite: TBE
 - Livret: TBE
 - Cartouche: TBE
[spoiler=Photos][url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Switch/windjammers_1.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Switch/windjammers_1.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Switch/windjammers_2.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Switch/windjammers_2.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Switch/windjammers_3.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Switch/windjammers_3.jpg[/img][/url][/spoiler]

[size=24][b]Wii[/b][/size]
[b]Ghost Squad Pack Intervention[/b]
TBE
 - Boite: TBE
 - Livret: TBE
 - CD: Nickel
[spoiler=Photos][url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/ghost_squad_1.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/ghost_squad_1.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/ghost_squad_10.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/ghost_squad_10.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/ghost_squad_11.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/ghost_squad_11.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/ghost_squad_12.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/ghost_squad_12.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/ghost_squad_13.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/ghost_squad_13.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/ghost_squad_2.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/ghost_squad_2.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/ghost_squad_3.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/ghost_squad_3.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/ghost_squad_4.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/ghost_squad_4.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/ghost_squad_5.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/ghost_squad_5.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/ghost_squad_6.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/ghost_squad_6.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/ghost_squad_7.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/ghost_squad_7.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/ghost_squad_8.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/ghost_squad_8.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/ghost_squad_9.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/ghost_squad_9.jpg[/img][/url][/spoiler]
[b]No More Heroes[/b]
Near mint
 - Boite: TBE (1 petit poc en bas � droite devant)
 - Livret: Nickel
 - CD: Nickel
 - VIP: absents
 - Autre: Infos
[spoiler=Photos][url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/no_more_heroes_2_1.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/no_more_heroes_2_1.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/no_more_heroes_2_2.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/no_more_heroes_2_2.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/no_more_heroes_2_3.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/no_more_heroes_2_3.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/no_more_heroes_2_4.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/no_more_heroes_2_4.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/no_more_heroes_2_5.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/no_more_heroes_2_5.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/no_more_heroes_2_6.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/no_more_heroes_2_6.jpg[/img][/url][/spoiler]
[b]Sin and Punishment[/b]
Near mint
 - Boite: TBE (1 petit poc en bas � droite devant)
 - Livret: Nickel
 - CD: Nickel
 - VIP: non gratt�s
 - Autre: Infos, pub
[spoiler=Photos][url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/sin_and_punishment_1.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/sin_and_punishment_1.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/sin_and_punishment_2.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/sin_and_punishment_2.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/sin_and_punishment_3.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/sin_and_punishment_3.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/sin_and_punishment_4.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/sin_and_punishment_4.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/sin_and_punishment_5.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/sin_and_punishment_5.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/sin_and_punishment_6.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/sin_and_punishment_6.jpg[/img][/url][/spoiler]
[b]Sonic Racing[/b]
EM/BE
 - Boite: EM/BE (coupure devant, et frottements)
 - Livret: TBE
 - CD: �ray�
 - VIP: absents
 - Autre: Infos
[spoiler=Photos][url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/sonic_racing_1.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/sonic_racing_1.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/sonic_racing_2.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/sonic_racing_2.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/sonic_racing_3.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/sonic_racing_3.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/sonic_racing_4.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/sonic_racing_4.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/sonic_racing_5.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/sonic_racing_5.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/sonic_racing_6.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/sonic_racing_6.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/sonic_racing_7.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii/sonic_racing_7.jpg[/img][/url][/spoiler]

[size=24][b]Wii U[/b][/size]
[b]Ninja Gaiden[/b]
TBE
 - Boite: TBE
 - Livret: Nickel
 - CD: Nickel
 - VIP: non gratt�s
 - Autre: Infos, guide rapide
[spoiler=Photos][url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii%20U/ninja_gaiden_3_1.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii U/ninja_gaiden_3_1.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii%20U/ninja_gaiden_3_2.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii U/ninja_gaiden_3_2.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii%20U/ninja_gaiden_3_3.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii U/ninja_gaiden_3_3.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii%20U/ninja_gaiden_3_4.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii U/ninja_gaiden_3_4.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii%20U/ninja_gaiden_3_5.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii U/ninja_gaiden_3_5.jpg[/img][/url]
[url=https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii%20U/ninja_gaiden_3_6.jpg][img]https://gitlab.com/t.guillon/video-games-photos/-/raw/master/Wii U/ninja_gaiden_3_6.jpg[/img][/url][/spoiler]

Encore merci, les jeunes, et � bient�t!
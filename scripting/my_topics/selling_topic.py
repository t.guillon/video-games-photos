""" Topic for selling stuff """
import pathlib
import sys

HEADDIR = pathlib.Path(__file__).parents[1]
SOURCEDIR = HEADDIR.parent

sys.path.append(str(HEADDIR))
import load_database
import make_topic as mt

database = SOURCEDIR / 'description.xlsx'
repo = load_database.files_repo

games = load_database.extract_database(database)
sold = games['sold_to'].apply(lambda elem: elem is not None)
games_sold = games[sold].reset_index(drop=True)
games = games[~sold].reset_index(drop=True)
content_sold = load_database.get_games_description(games_sold)
content = load_database.get_games_description(games)

warning_holidays = mt.BBText(
    "Topic en pause le temps des vacances de Noël 2023",
    b=None,
    size=16,
    color='red'
    )

warning1 = mt.BBText(
    "Malgré un petit tour sur le net, y compris sur le fofo, je me suis"
    " peut-être planté sur les prix."
    ).bolded
warning2 = mt.BBText(
    "Si c'est trop cher, n'hésitez pas à me le signaler même si vous ne"
    " souhaitez pas acheter mais juste m'informer."
    ).bolded
notabene = mt.BBText(
    "En en-tête de chaque console, il y a un lien \"Toutes les photos\""
    " qui renvoie vers un dossier avec... bah, toutes les photos."
    ).underlined

header = f"""

Salut tout le monde.

Allez, je dégraisse un peu ma collection.

Merci de vous manifester sur le topic d'abord et de garder le MP pour la finalisation.
Les prix sont fdpout, et on peut tout à fait discuter pour les prix de lot.
Paiement Paypal, virement, chèque ou liquide si MP sur Orléans.

{warning1}
{warning2}

J'ai essayé d'être aussi exhaustif que possible sur les états :
- TBE : très bon état
- BE : bon état
- EM : état moyen
- ME : mauvais état
J'ai ajouté pas mal de photos, mais n'hésitez pas à m'en demander plus si vous le souhaitez.
{notabene}
Les photos sont prises pour tâcher de faire ressortir au max les rayures (notamment sur les CDs), le rendu est moins dur en vrai.

"""

footer = """

Merci de m'avoir lu, et à plus tard!

"""

inner = []
for machine, items in content.items():
    if items:
        section = [
            str(mt.BBText(machine).bolded.sized(24)),
            '{} {} {}'.format(
                mt.CAMERA,
                mt.BBText(
                    f'Toutes les photos {machine}',
                    url = load_database.get_support_url(machine, repo)
                    ),
                mt.CAMERA,
                )
            ]
        for item in items:
            if item.photos:
                item.photos = item.photos[:1]
            if isinstance(item, mt.BundleDescription):
                for game in item.games:
                    if game.photos:
                        game.photos = game.photos[:1]
        section.extend([
            '\n'.join(str(item) for item in items),
            ''
            ])
        inner.extend(section)
        
# NB: for sold items, photos are turned off
inner_sold = []
for machine, items in content_sold.items():
    if items:
        section = [
            str(mt.BBText(machine).bolded.sized(24)),
            '{} {} {}'.format(
                mt.CAMERA,
                mt.BBText(
                    f'Toutes les photos {machine}',
                    url = load_database.get_support_url(machine, repo)
                    ),
                mt.CAMERA,
                )
            ]
        for item in items:
            if item.photos:
                item.photos = None
            if isinstance(item, mt.BundleDescription):
                for game in item.games:
                    if game.photos:
                        game.photos = None
        section.extend([
            '\n'.join(str(item) for item in items),
            ''
            ])
        inner_sold.extend(section)

topic = (
    header
    + '\n'.join(inner)
    + '\n'
    + str(mt.BBText('\n'.join(inner_sold), spoiler = 'VENDUS'))
    + footer
    )

with open('selling_topic.txt', 'w') as wt:
    wt.write(topic)
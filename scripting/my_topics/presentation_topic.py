""" Creating the code creation topic """
import pathlib
import sys

HEADDIR = pathlib.Path(__file__).parents[1]
SOURCEDIR = HEADDIR.parent

sys.path.append(str(HEADDIR))
import load_database
from make_topic import *

repo = 'https://gitlab.com/t.guillon/video-games-photos/-'

test = BBText('Salut la compagnie', b=None, size=18)
test_code = [
    f'>>> test = {repr(test)}',
    '>>> print(test)',
    str(test)
    ]
test2 = BBText('Bienvenue chez les fous')
fofo_url = 'https://www.gamopat-forum.com/'
test2_code = [
    f'>>> test2 = {repr(test2)}',
    f'>>> print(test2.hyperlinked(url="{fofo_url}"))',
    str(test2.hyperlinked(url=fofo_url))
    ]
test3 = GameDescription(
    name='Story Of Thor',
    machine='MD',
    warning='jeu en allemand',
    state_overall='TBE',
    state_box='BE/TBE',
    state_booklet='BE/TBE',
    state_cartridge='BE/TBE',
    other_items="Feuille d'erratum",
    prices = [35,32,28],
    photos=[f'{repo}/raw/master/MD/story_of_thor_1.jpg']
    )
test3_code = [
    f'>>> test3 = {repr(test3)}',
    '>>> print(test3)',
    str(test3)
    ]

topic = f"""
Salut tout le monde!

Récemment, j'ai voulu mettre en ventes quelques jeux ici-même.
J'avais un (très) mauvais souvenir de l'écriture des topics de vente sur le fofo {BBText('dès lors que le contenu devenait conséquent', 'u')}.
Je me suis retrouvé plus d'une fois à perdre une bonne partie de mon topic et à devoir tout recommencer...
J'ai alors créé un code pour générer mes topics d'estim/vente automatiquement à partir d'une petite base de données (un fichier Excel).

Pas sûr que ça serve à grand monde, mais je mets quand même ici un lien vers le dépôt :
{BBText(f'{repo}/tree/master').urled}

Dans les grandes lignes, voilà le contenu du code :

L'objet le plus simple sert juste à gérer le BBCode.
Il suffit de lui donner le texte à afficher, et toutes les options associées.
Par exemple :
{python_excerpt(test_code)}
permet de créer le texte suivant :

{test_code[-1]}

On peut aussi créer des objets simples d'abord, puis enrichir avec des codes ensuite :
{python_excerpt(test2_code)}
ce qui donne :

{test2_code[-1]}

Comme dit plus haut, l'objectif final pour moi est de générer automatiquement mes topics d'estimation et de vente.
À partir de là, les objets ont un gros parti pris pour le rendu que je veux avoir!
{BBText('In fine').italicized}, je déclare mes jeux comme ça :
{python_excerpt(test3_code[0])}
ce qui génère :

{str(test3)}

La déclaration est carrément simplifiée car il faudrait sinon que je fasse tout à la main (x N jeux, bien sûr), ou alors que j'écrive directement la ligne BBCode correspondante :
{python_excerpt(test3_code[1:])}

Et oualà.

À plus!

PS : le topic a été automatiquement créé à partir de ce {BBText('script-là', url=f'{repo}/blob/master/scripting/my_topics/presentation_topic.py')}.

"""
with open('presentation_topic.txt', 'w') as wt:
    wt.write(topic)
""" Automatic creation of description files from database """
import pathlib

import pandas as pd

CWD = pathlib.Path(__file__).parent
HEADDIR = CWD.parent

database = HEADDIR/'description.xlsx'


df = pd.read_excel(database)
df.dropna(how='all', axis=0, inplace=True)
supports = df['Support'][df['Support'].notna()]
df['Support'].fillna(method='ffill', inplace=True)
sold = df['Vendu'].notna()
df['Pas à vendre'][sold] = 'x'
iprice = df.columns.get_loc('Prix')
price_cols = df.columns[iprice:]
cols_to_drop = ['Vendu', 'Réservé']+list(price_cols)
df.drop(cols_to_drop, inplace=True, axis=1)

readme_name = 'Readme.md'
readme_sub_name = 'état_des_jeux_{support}.md'
# Main Readme.md
lines = []
for support in supports:
    sup_no_sp = support.replace(' ', '')
    support = support.replace(' ', r'\ ')
    line = f' - [{support}]({support}/{readme_sub_name.format(support=sup_no_sp)})'
    lines.append(line)
lines = '\n'.join(lines)
content = f"""
## À propos

Quelques jeux-vidéo à la vente, voir les photos et fichiers de description correspondants :

{lines}

## Bonus

Un bout de code Python pour générer mes topics de vente [Gamopat](https://www.gamopat-forum.com) dans le dossier [scripting](scripting).

Y'a un objet pratique pour gérer le BBCode sans avoir à tout éditer à la main :
```python
>>> test = BBText('Salut', 'b', color='red')
>>> print(test)
[color=red][b]Salut[/b][/color]
```

Il est possible de mettre à jour le texte avec des attributs :
```python
>>> test2 = BBText('Salut')
>>> print(test2.bolded.colored('red'))
[color=red][b]Salut[/b][/color]
```

C'est pratique quand y'a beaucoup d'options à mettre :
```python
>>> test3 = BBText(
    'Welcome to this GitLab project!',
    b=None,
    i=None,
    u=None,
    size=12,
    color='#0000ff',
    url='https://gitlab.com/t.guillon/video-games-photos',
    spoiler='Caché'
    )
>>> test3
BBText("Welcome to this GitLab project!", b=None, i=None, u=None, size=12, color="#0000ff", url="https://gitlab.com/t.guillon/video-games-photos", spoiler="Caché")
>>> str(test3)
'[spoiler=Caché][url=https://gitlab.com/t.guillon/video-games-photos][color=#0000ff][size=12][u][i][b]Welcome to this GitLab project![/b][/i][/u][/size][/color][/url][/spoiler]'

```

## Licence

Pas de licence particulière, pensez juste à faire une citation!

À plus

"""

with (HEADDIR/readme_name).open('w', encoding='utf8') as wt:
    wt.write(content)

# Sub Readmes
template = [
    '## À propos',
    '',
    'État général et contenu des jeux **{support}**.',
    '',
    'États:',
    '',
    ' - ME : mauvais état',
    ' - EM : état moyen',
    ' - BE : bon état',
    ' - TBE : très bon état',
    ' - near mint : bah, near mint quoi',
    '',
    "Le jugé des états reste subjectif, le mieux est d'aller voir les photos dans le dossier correspondant.",
    '',
    '## Description',
    '',
    '{table}'
    ]
template='\n'.join(template)

for support, df_sub in df.groupby('Support'):
    df_sub.drop(['Support'], axis=1, inplace=True)
    table = df_sub.to_html(
        na_rep='', bold_rows=True, justify='left', index=False
        )
    content = template.format(support=support, table=table)
    sup_no_sp = support.replace(' ', '')
    outfile = HEADDIR/support/readme_sub_name.format(support=sup_no_sp)
    with outfile.open('w', encoding='utf8') as wt:
        wt.write(content)
        
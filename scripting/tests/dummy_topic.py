""" Some trial and errors for creating a topic """

import pathlib
import sys

HEADDIR = pathlib.Path(__file__).parents[1]

sys.path.append(str(HEADDIR))
import make_topic

header = """
    Salut la compagnie.
    
    Bienvenue dans mon topic, on va bien déconner.

    """

footer = """

    Merci de m'avoir lu, et à plus tard!

    """

src = 'https://gitlab.com/t.guillon/video-games-photos/-/raw/master/GC/battalion_wars_1.jpg'
img = make_topic.BBText(src, img='133x100', url=src)
wthout = make_topic.BBText('sans')
wth = make_topic.BBText('avec')
mess1 = f'Voici une image {wthout.bolded} spoiler:'
mess2 = f'Et une image {wthout.striked}{wth.bolded.sized(14).colored("red")}:'

inner = [
    str(line)
    for line in (mess1, '', img, '', mess2, img.spoiling('Cachée!'))
    ]

header = [line.strip() for line in header.split('\n')]
footer = [line.strip() for line in footer.split('\n')]

content = header + inner + footer

topic = '\n'.join(content)
print(topic)
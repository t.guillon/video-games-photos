""" The demo topic """
import pathlib
import sys

HEADDIR = pathlib.Path(__file__).parents[1]
SOURCEDIR = HEADDIR.parent

sys.path.append(str(HEADDIR))
import load_database
import make_topic as mt

database = 'cas_test.xlsx'
repo = load_database.files_repo

games = load_database.extract_database(database)
content = load_database.get_games_description(games)

# # 1st layout: all photos
# print('#############')
# print('First layout')
# print('#############')
# inner = []
# for machine, items in content.items():
    # if items:
        # section = [
            # str(mt.BBText(machine).bolded.sized(24)),
            # '\n'.join(str(item) for item in items),
            # ''
            # ]
        # inner.extend(section)
# print('\n'.join(inner))

# 2nd layout: 1st photo only + link to repo
print('#############')
print('Second layout')
print('#############')
inner = []
for machine, items in content.items():
    if items:
        section = [
            str(mt.BBText(machine).bolded.sized(24)),
            str(mt.BBText(
                'Toutes les photos',
                url = load_database.get_support_url(machine, repo),
                size=10
                ))
            ]
        for item in items:
            if item.photos:
                item.photos = item.photos[:1]
            if isinstance(item, mt.BundleDescription):
                for game in item.games:
                    if game.photos:
                        game.photos = game.photos[:1]
        section.extend([
            '\n'.join(str(item) for item in items),
            ''
            ])
        inner.extend(section)
print('\n'.join(inner))
""" Loading the custom database (=xlsx file)

It relies on my own standards.

Any loading from other databases will need a whole new set of functions.

"""
import pathlib
import re
import sys

import pandas as pd

CWD = pathlib.Path(__file__).parent
SOURCEDIR = CWD.parent

import make_topic as mt

vip_pattern = dict(oui='non grattés', grattés='grattés', non='absents')
caution_pattern = re.compile('(?P<bef>[^\(]+)(?P<caution>[^\)]*)(?P<aft>.*)')
photos_repo = 'https://gitlab.com/t.guillon/video-games-photos/-/raw/master'
files_repo = 'https://gitlab.com/t.guillon/video-games-photos/-/tree/master'

def extract_caution(name):
    """ Extract actual game name and caution message from a game name """
    items = caution_pattern.match(name)
    caution = items['caution']
    if caution:
        name = (items['bef']+items['aft'][1:]).strip()
        caution = caution[1:].strip()
    else:
        caution = None # '', initially
    return name, caution

def transform_vip(vip):
    """ Modifying VIP points label """
    return vip_pattern.get(vip)
    
def get_support_url(support, repo):
    """ Getting source url of a support console """
    return f'{repo}/{support}'.replace(' ', mt.HTML_SPACE)
    
def get_photo_urls(name, support, repo, source_dir):
    """ Finding all photos attached to a given game """
    urls = [
        f'{repo}/{support}/{photo.name}'.replace(' ', mt.HTML_SPACE)
        if photo
        else None
        for photo in (source_dir/support).glob(f"{name}*.jpg")
        ]
    if urls:
        return urls

def extract_database(database):
    """ Extracting games from an xlsx database
    
    Note
    ----
    As mentioned at head of the py source, extraction relies on my pure
    arbitrary convention.
    
    Parameters
    ----------
    database: str/pathlib.Path,
        Full path to xlsx database
        
    Returns
    -------
    out: pd.DataFrame,
        The games arranged in a dataframe
    
    """
    df = pd.read_excel(database)
    # Drop emptylines
    df.dropna(how='all', axis=0, inplace=True)
    # Propagate support machines
    df['Support'].fillna(method='ffill', inplace=True)
    # Removing not for sale
    not_for_sale = df['Pas à vendre'].notna()
    indices = df[not_for_sale].index
    df.drop(indices, axis=0, inplace=True)
    df.drop(['Pas à vendre'], axis=1, inplace=True)
    nrows, ncols = df.shape
    # Prices
    has_prices = 'Prix' in df
    if has_prices:
        iprice = df.columns.get_loc('Prix')
        has_revised_prices = ncols>iprice+1
        if has_revised_prices:
            prices = df.iloc[:,iprice:]
            prices = prices.apply(
                lambda values: [val for val in values if pd.notna(val)],
                axis=1
                )
            df['Prix'] = prices
            df.drop(columns=df.columns[iprice+1:], inplace=True)
    else:
        df['Prix'] = None
    # Names and warnings
    df['Jeu'], df['Attention'] = zip(*df['Jeu'].apply(extract_caution))
    # VIP
    df['VIP'] = df['VIP'].apply(transform_vip)
    # Photos
    df['Photo'] = df[['Photo', 'Support']].apply(
        lambda row: get_photo_urls(row[0], row[1], photos_repo, SOURCEDIR),
        axis=1
        )
    # Fetching bundles
    in_bundle = df['Jeu'].str.strip().str.startswith('-')
    deltas = in_bundle.astype(int).diff()
    starts = (deltas==1).shift(-1) # shift since bundle starts before the 1st '-'
    ends = (deltas==-1).shift(-1)
    starts.iloc[0] = in_bundle.iloc[0]
    starts.iloc[-1] = False
    ends.iloc[-1] = in_bundle.iloc[-1]
    df['is_bundle_head'] = starts
    df['is_bundle_last'] = ends
    for i, (start,end) in enumerate(zip(starts[starts].index,ends[ends].index)):
        df.loc[start:end,'Vendu'] = df['Vendu'].loc[start]
        df.loc[start:end,'Réservé'] = df['Réservé'].loc[start]
    # Final dataframe
    header_mapping = dict(
        Jeu = 'name',
        Support = 'machine',
        Attention = 'warning',
        Général = 'state_overall',
        Boite = 'state_box',
        Livret = 'state_booklet',
        CD = 'state_cd',
        Cartouche = 'state_cartridge',
        Autre = 'other_items',
        VIP = 'vip_card',
        Photo = 'photos',
        Vendu = 'sold_to',
        Réservé = 'booked_by',
        Prix = 'prices',
        is_bundle_head = 'is_bundle_head',
        is_bundle_last = 'is_bundle_last',
        )
    games = pd.DataFrame(
        df[header_mapping.keys()].values,
        columns = header_mapping.values()
        )
    games[games.isna()] = None
    return games

def get_games_description(games):
    """ Getting games descriptions from a dataset
    
    Parameters
    ----------
    games: pd.DataFrame,
        The games dataset
        
    Returns
    -------
    out: dict,
        The games and bundles with key=games machines and vals=list of
        associated games/bundles
    """
    descriptions = dict()
    for machine, games_sub in games.groupby('machine'):
        in_bundle = False
        items = []
        for _, game in games_sub.iterrows():
            is_bhead = game.pop('is_bundle_head')
            is_blast = game.pop('is_bundle_last')
            if is_bhead:
                bundle = dict(
                    name=game['name'],
                    warning=game['warning'],
                    state=game['state_overall'],
                    photos=game['photos'],
                    sold_to=game['sold_to'],
                    booked_by=game['booked_by'],
                    prices=game['prices']
                    )
                bundle_games = []
                in_bundle = True
                continue
            elif in_bundle:
                game['sold_to'] = bundle['sold_to']
                game['booked_by'] = bundle['booked_by']
                bundle_games.append(mt.GameDescription(**dict(game)))
                if is_blast:
                    bundle['games'] = bundle_games
                    items.append(mt.BundleDescription(**bundle))
                    in_bundle=False
                continue
            items.append(mt.GameDescription(**dict(game)))
        if not items:
            items = None
        descriptions[machine] = items
    return descriptions
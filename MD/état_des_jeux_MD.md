## À propos

État général et contenu des jeux **MD**.

États:

 - ME : mauvais état
 - EM : état moyen
 - BE : bon état
 - TBE : très bon état
 - near mint : bah, near mint quoi

Le jugé des états reste subjectif, le mieux est d'aller voir les photos dans le dossier correspondant.

## Description

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: left;">
      <th>Jeu</th>
      <th>Général</th>
      <th>Boite</th>
      <th>Livret</th>
      <th>CD</th>
      <th>Cartouche</th>
      <th>Autre</th>
      <th>VIP</th>
      <th>Photo</th>
      <th>Pas à vendre</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Castle of Illusion</td>
      <td>TBE</td>
      <td>TBE</td>
      <td></td>
      <td></td>
      <td>TBE</td>
      <td></td>
      <td></td>
      <td>castle_of_illusion</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Chakan</td>
      <td>ME/EM</td>
      <td>ME (plastiquette et jaquette arrières fendus)</td>
      <td>ME (scotché)</td>
      <td></td>
      <td>BE (léger renfoncement en haut de l'étiquette)</td>
      <td></td>
      <td></td>
      <td>chakan</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Comix Zone</td>
      <td>TBE</td>
      <td>TBE</td>
      <td></td>
      <td></td>
      <td>TBE</td>
      <td></td>
      <td></td>
      <td>comix_zone</td>
      <td>x</td>
    </tr>
    <tr>
      <td>David Robinson's Supreme Court</td>
      <td>TBE</td>
      <td>TBE</td>
      <td>TBE</td>
      <td></td>
      <td>TBE</td>
      <td></td>
      <td></td>
      <td>david_robinson</td>
      <td>x</td>
    </tr>
    <tr>
      <td>DBZ L'appel du destin</td>
      <td>BE/TBE</td>
      <td></td>
      <td></td>
      <td></td>
      <td>quelques griffures à l'arrière + étiquette peut-être 1 peu sunfadée</td>
      <td></td>
      <td></td>
      <td>dbz</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Fifa 97</td>
      <td>TBE</td>
      <td>TBE</td>
      <td>BE/TBE (coins un peu cornés)</td>
      <td></td>
      <td>Nickel</td>
      <td></td>
      <td></td>
      <td>fifa</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Flashback</td>
      <td>EM/BE</td>
      <td>ME (plastique en haut de la tranche fendu + jaquette déchirée en bas de la tranche)</td>
      <td></td>
      <td></td>
      <td>TBE</td>
      <td></td>
      <td></td>
      <td>flashback</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Gain Ground</td>
      <td>BE/TBE</td>
      <td>BE (aspect un peu vieilli)</td>
      <td></td>
      <td></td>
      <td>TBE</td>
      <td></td>
      <td></td>
      <td>gain_ground</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Global Gladiators</td>
      <td>BE</td>
      <td>ME (arrière fendu + haut de la tranche cassé)</td>
      <td></td>
      <td></td>
      <td>TBE</td>
      <td></td>
      <td></td>
      <td>global_gladiators</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Jungle Strike (PAL mais réfs américaines partout + livret en anglais)</td>
      <td>TBE</td>
      <td>BE (jaquette 1 peu déchirée avant haut + 1 coin cassé + 1 attache de cartouche un peu molle)</td>
      <td>TBE</td>
      <td></td>
      <td>Nickel</td>
      <td>Reg card EA, pub</td>
      <td></td>
      <td>jungle_strike</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Mortal Kombat</td>
      <td>TBE</td>
      <td>BE (sauf 1 fissure sur la charnière)</td>
      <td>TBE</td>
      <td></td>
      <td>Nickel</td>
      <td></td>
      <td></td>
      <td>mortal_kombat</td>
      <td>x</td>
    </tr>
    <tr>
      <td>NBA Jam Tournament Edition</td>
      <td>TBE</td>
      <td>TBE</td>
      <td>EM/BE (plis)</td>
      <td></td>
      <td>TBE</td>
      <td></td>
      <td></td>
      <td>nba_jam</td>
      <td></td>
    </tr>
    <tr>
      <td>Olympic Gold</td>
      <td>BE/TBE</td>
      <td></td>
      <td></td>
      <td></td>
      <td>quelques griffures à l'arrière</td>
      <td></td>
      <td></td>
      <td>olympic_gold</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Pugsy</td>
      <td>EM</td>
      <td>ME (trou à l'arrière + plastique déchiré)</td>
      <td>ME (scotché)</td>
      <td></td>
      <td>BE (quelques griffures à l'arrière)</td>
      <td></td>
      <td></td>
      <td>pugsy</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Story Of Thor (ALLEMAND!)</td>
      <td>TBE</td>
      <td>TBE (1 mini trou dans le plastique sur la tranche)</td>
      <td>BE/TBE</td>
      <td></td>
      <td>BE/TBE (une trace de pli dans un coin de l'étiquette)</td>
      <td>Feuille d'erratum</td>
      <td></td>
      <td>story_of_thor</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Street Fighter II Special Champion Edition</td>
      <td>TBE</td>
      <td>TBE (mais plastique 1 peu fendu coin avant haut)</td>
      <td>BE</td>
      <td></td>
      <td>TBE</td>
      <td></td>
      <td></td>
      <td>street_fighter</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Sunset Riders</td>
      <td>TBE</td>
      <td></td>
      <td></td>
      <td></td>
      <td>TBE</td>
      <td></td>
      <td></td>
      <td>sunset_riders</td>
      <td>x</td>
    </tr>
    <tr>
      <td>TMNT The Hyperstone Heist</td>
      <td>BE</td>
      <td></td>
      <td></td>
      <td></td>
      <td>un poc avant bas, étiquette un peu passée sur l'angle</td>
      <td></td>
      <td></td>
      <td>tmnt</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Urban Strike</td>
      <td>TBE</td>
      <td>BE/TBE (1 languette qui retien la cartouche qui ne tient plus)</td>
      <td>TBE</td>
      <td></td>
      <td>TBE</td>
      <td></td>
      <td></td>
      <td>urban_strike</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Virtua Racing</td>
      <td>BE</td>
      <td></td>
      <td></td>
      <td></td>
      <td>quelques griffures devant/derrière</td>
      <td></td>
      <td></td>
      <td>vr</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Wolverine</td>
      <td>TBE</td>
      <td></td>
      <td></td>
      <td></td>
      <td>TBE</td>
      <td></td>
      <td></td>
      <td>wolverine</td>
      <td>x</td>
    </tr>
  </tbody>
</table>
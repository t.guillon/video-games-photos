## À propos

État général et contenu des jeux **Wii**.

États:

 - ME : mauvais état
 - EM : état moyen
 - BE : bon état
 - TBE : très bon état
 - near mint : bah, near mint quoi

Le jugé des états reste subjectif, le mieux est d'aller voir les photos dans le dossier correspondant.

## Description

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: left;">
      <th>Jeu</th>
      <th>Général</th>
      <th>Boite</th>
      <th>Livret</th>
      <th>CD</th>
      <th>Cartouche</th>
      <th>Autre</th>
      <th>VIP</th>
      <th>Photo</th>
      <th>Pas à vendre</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Gaine Wiimote noire</td>
      <td>BE</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td>gaine</td>
      <td></td>
    </tr>
    <tr>
      <td>Ghost Squad Pack Intervention</td>
      <td>TBE</td>
      <td>TBE</td>
      <td>TBE</td>
      <td>Nickel</td>
      <td></td>
      <td>Boite TBE, plastique pour zapper BE (1 peu déformé sur un coin), zapper TBE, pubs)</td>
      <td></td>
      <td>ghost_squad</td>
      <td>x</td>
    </tr>
    <tr>
      <td>No More Heroes</td>
      <td>Near mint</td>
      <td>TBE (1 petit poc en bas à droite devant, et jaquette légèrement déformée juste sous le poc)</td>
      <td>Nickel</td>
      <td>Nickel</td>
      <td></td>
      <td>Infos</td>
      <td>non</td>
      <td>no_more_heroes_2</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Sin and Punishment</td>
      <td>Near mint</td>
      <td>TBE (1 petit poc en bas à droite devant, et jaquette un tout petit peu déchirée juste sous le poc)</td>
      <td>Nickel</td>
      <td>Nickel</td>
      <td></td>
      <td>Infos, pub</td>
      <td>oui</td>
      <td>sin_and_punishment</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Sonic Racing</td>
      <td>EM/BE</td>
      <td>EM/BE (coupure devant, et frottements)</td>
      <td>TBE</td>
      <td>µrayé</td>
      <td></td>
      <td>Infos</td>
      <td>non</td>
      <td>sonic_racing</td>
      <td>x</td>
    </tr>
  </tbody>
</table>
## À propos

État général et contenu des jeux **N64**.

États:

 - ME : mauvais état
 - EM : état moyen
 - BE : bon état
 - TBE : très bon état
 - near mint : bah, near mint quoi

Le jugé des états reste subjectif, le mieux est d'aller voir les photos dans le dossier correspondant.

## Description

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: left;">
      <th>Jeu</th>
      <th>Général</th>
      <th>Boite</th>
      <th>Livret</th>
      <th>CD</th>
      <th>Cartouche</th>
      <th>Autre</th>
      <th>VIP</th>
      <th>Photo</th>
      <th>Pas à vendre</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Banjo Kazooie</td>
      <td>TBE</td>
      <td></td>
      <td></td>
      <td></td>
      <td>TBE</td>
      <td></td>
      <td></td>
      <td>banjo</td>
      <td>x</td>
    </tr>
    <tr>
      <td>F Zero X</td>
      <td>TBE</td>
      <td></td>
      <td></td>
      <td></td>
      <td>TBE</td>
      <td></td>
      <td></td>
      <td>f_zero</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Lylat Wars</td>
      <td>TBE</td>
      <td></td>
      <td></td>
      <td></td>
      <td>TBE</td>
      <td></td>
      <td></td>
      <td>lylat</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Mario Kart</td>
      <td>TBE</td>
      <td></td>
      <td></td>
      <td></td>
      <td>TBE</td>
      <td></td>
      <td></td>
      <td>mario</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Silicon valley</td>
      <td>TBE</td>
      <td></td>
      <td></td>
      <td></td>
      <td>TBE</td>
      <td></td>
      <td></td>
      <td>space_station_silicon</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Rogue Squadron</td>
      <td>TBE</td>
      <td></td>
      <td></td>
      <td></td>
      <td>TBE</td>
      <td></td>
      <td></td>
      <td>star_wars_rogue</td>
      <td>x</td>
    </tr>
    <tr>
      <td>Yoshi's Story</td>
      <td>TBE</td>
      <td></td>
      <td></td>
      <td></td>
      <td>TBE</td>
      <td></td>
      <td></td>
      <td>yoshi</td>
      <td>x</td>
    </tr>
  </tbody>
</table>